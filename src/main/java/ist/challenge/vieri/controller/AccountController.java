package ist.challenge.vieri.controller;

import ist.challenge.vieri.dto.AccountRequest;
import ist.challenge.vieri.dto.DataResponse;
import ist.challenge.vieri.model.Account;
import ist.challenge.vieri.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class AccountController {

  @Autowired
  AccountService accountService;

  @RequestMapping(value = "/register", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> register(@RequestBody AccountRequest request) {
    return accountService.register(request);
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> login(@RequestBody AccountRequest request) {
    return accountService.login(request);
  }

  @RequestMapping(value = "/user", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<DataResponse<Account>> user(@RequestParam(required = false, defaultValue = "") String search,
                                                    @RequestParam(required = false, defaultValue = "0") String page) {
    int pageNumber = Integer.parseInt(page);
    return accountService.getUser(search, pageNumber);
  }

  @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT,  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> update(@PathVariable Long id, @RequestBody AccountRequest request) {
    return accountService.update(id, request);
  }


  //Testing 1



}