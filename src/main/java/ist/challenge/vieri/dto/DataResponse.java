package ist.challenge.vieri.dto;

import java.util.List;

public class DataResponse<T> {
  private List<T> results;
  private String resmsg;
  private Long count;

  public DataResponse(List<T> results, String resmsg) {
    this.results = results;
    this.resmsg = resmsg;
  }

  public DataResponse(List<T> results, String resmsg, Long count) {
    this.results = results;
    this.resmsg = resmsg;
    this.count = count;
  }

  public List<T> getResults() {
    return results;
  }

  public void setResults(List<T> results) {
    this.results = results;
  }

  public String getResmsg() {
    return resmsg;
  }

  public void setResmsg(String resmsg) {
    this.resmsg = resmsg;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }
}
