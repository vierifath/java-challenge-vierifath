package ist.challenge.vieri.model;

import javax.persistence.*;

@Entity
@Table(name = "Users")
public class Account {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "username", nullable = false, length = 25, unique = true, columnDefinition = "VARCHAR(25)")
//  @NotEmpty(message = "Username cannot be empty")
  private String username;

  @Column(name = "password", nullable = false, length = 100, columnDefinition = "VARCHAR(100)")
  private String password;

  public Account() {
  }

  public Account(Long id, String username, String password) {
    this.id = id;
    this.username = username;
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}

// Testing merging
// Fitur 2.1
// Fitur 3.1
// Fitur 4.1
// Fitur 4.2
// Fitur 5.1
// Fitur 6