package ist.challenge.vieri.repository;

import ist.challenge.vieri.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
  Account findByUsername(String username);

  Page<Account> findByUsernameIgnoreCaseContaining(String username, Pageable pageable);

  long countByUsernameIgnoreCaseContaining(@Param("search") String search);

}
