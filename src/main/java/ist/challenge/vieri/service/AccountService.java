package ist.challenge.vieri.service;

import ist.challenge.vieri.dto.AccountRequest;
import ist.challenge.vieri.dto.DataResponse;
import ist.challenge.vieri.model.Account;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountService {
  public ResponseEntity<String> register(AccountRequest request);
  public ResponseEntity<String> login(AccountRequest request);
  public ResponseEntity<DataResponse<Account>> getUser(String search, int page);
  public ResponseEntity<String> update(Long id, AccountRequest request);

}
