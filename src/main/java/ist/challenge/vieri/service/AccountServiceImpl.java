package ist.challenge.vieri.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import ist.challenge.vieri.dto.AccountRequest;
import ist.challenge.vieri.dto.DataResponse;
import ist.challenge.vieri.dto.RegistrationResponse;
import ist.challenge.vieri.model.Account;
import ist.challenge.vieri.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

  @Autowired
  AccountRepository accountRepository;

  @Autowired
  BCryptPasswordEncoder encoder;

  @Override
  public ResponseEntity<String> register(AccountRequest request) {

    RegistrationResponse response = new RegistrationResponse();

    JsonObject resmsg = new JsonObject();
    Gson gson = new Gson();

    Account account = new Account();
    account.setUsername(request.getUsername());
    account.setPassword(encoder.encode(request.getPassword()));

    try {
      //Step 1: Check Username Availability
      if (accountRepository.findByUsername(request.getUsername()) != null) {
        response.setMessage("Username sudah terpakai");
        response.setStatus(HttpStatus.CONFLICT);
      } else {
        accountRepository.save(account);
        response.setMessage("Registration Successful!");
        response.setStatus(HttpStatus.CREATED);
      }
    } catch (DataAccessException e) {
      response.setMessage("Database error occurred. Please try again later.");
      response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception ex) {
      response.setMessage("Connection timeout. Please try again in a few later.");
      response.setStatus(HttpStatus.GATEWAY_TIMEOUT);
    }

    resmsg.addProperty("resmsg", response.getMessage());

    return new ResponseEntity<>(gson.toJson(resmsg), response.getStatus());
  }

  @Override
  public ResponseEntity<String> login(AccountRequest request) {

    RegistrationResponse response = new RegistrationResponse();

    JsonObject resmsg = new JsonObject();
    Gson gson = new Gson();

    try {
      //Step 1: Check whether the account has been registered
      Account account = accountRepository.findByUsername(request.getUsername());
      if (account != null) {
        //Step 2: Check Authentication
        if (authenticationCheck(request.getPassword(), account.getPassword())) {
          response.setMessage("Sukses Login");
          response.setStatus(HttpStatus.OK);
        } else {
          response.setMessage("Your Password is invalid, please try again.");
          response.setStatus(HttpStatus.UNAUTHORIZED);
        }
      } else {
        response.setMessage("Your account is unregistered!");
        response.setStatus(HttpStatus.UNAUTHORIZED);
      }

    } catch (DataAccessException e) {
      response.setMessage("Database error occurred. Please try again later.");
      response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception ex) {
      response.setMessage("Connection timeout. Please try again in a few later.");
      response.setStatus(HttpStatus.GATEWAY_TIMEOUT);
    }

    resmsg.addProperty("resmsg", response.getMessage());

    return new ResponseEntity<>(gson.toJson(resmsg), response.getStatus());
  }

  @Override
  public ResponseEntity<DataResponse<Account>> getUser(String search, int page) {

    long totalCount;

    try {
      Pageable pageable = PageRequest.of(page, 10);
      Page<Account> accountPage;

      //Step 1: Fetching the data
      //Get data by search parameter
      if (search != null && !search.isEmpty()) {
        accountPage = accountRepository.findByUsernameIgnoreCaseContaining(search, pageable);
        totalCount = accountRepository.countByUsernameIgnoreCaseContaining(search);
      } else {
        //Get all data
        accountPage = accountRepository.findAll(pageable);
        totalCount = accountRepository.count();
      }

      //Handling Invalid Page
      if (page >= accountPage.getTotalPages()) {
        return new ResponseEntity<>(new DataResponse<Account>(null, "Invalid page number"), HttpStatus.NOT_FOUND);
      }

      List<Account> accounts = accountPage.getContent();

      return new ResponseEntity<>(new DataResponse<Account>(accounts, "Success", totalCount), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(new DataResponse<Account>(null, "Error occurred while processing the request"),
              HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  public ResponseEntity<String> update(Long id, AccountRequest request) {

    RegistrationResponse response = new RegistrationResponse();

    JsonObject resmsg = new JsonObject();
    Gson gson = new Gson();

    try {
      Optional<Account> getAccount = accountRepository.findById(id);
      //Step 1: Check Account
      if (getAccount.isPresent()) {
        Account existingAcc = getAccount.get();
        Account account = accountRepository.findByUsername(request.getUsername());

        //Step 2: Check Username Availability
        if (account == null || request.getUsername().equals(existingAcc.getUsername())) {
          existingAcc.setUsername(request.getUsername());

          //Step 3: Check Password
          if (!authenticationCheck(request.getPassword(), account.getPassword())) {
            account.setUsername(request.getUsername());
            account.setPassword(encoder.encode(request.getPassword()));

            //Step 4: Update data
            accountRepository.save(account);
            response.setMessage("Success to update data");
            response.setStatus(HttpStatus.CREATED);
          } else {
            response.setMessage("Password tidak boleh sama dengan sebelumnya");
            response.setStatus(HttpStatus.BAD_REQUEST);
          }
        } else {
          response.setMessage("Username sudah terpakai");
          response.setStatus(HttpStatus.CONFLICT);
        }
      } else {
        response.setMessage("Your Account is unregistered");
        response.setStatus(HttpStatus.UNAUTHORIZED);
      }

    } catch (DataAccessException e) {
      response.setMessage("Database error occurred. Please try again later.");
      response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception ex) {
      response.setMessage("Connection timeout. Please try again in a few later.");
      response.setStatus(HttpStatus.GATEWAY_TIMEOUT);
    }

    resmsg.addProperty("resmsg", response.getMessage());

    return new ResponseEntity<>(gson.toJson(resmsg), response.getStatus());
  }

  //Check Authentication
  public Boolean authenticationCheck(String newPass, String oldPass) {
    if (encoder.matches(newPass, oldPass)) {
      return true;
    } else {
      return false;
    }
  }
}
