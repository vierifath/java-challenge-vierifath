package ist.challenge.vieri;

import ist.challenge.vieri.dto.AccountRequest;
import ist.challenge.vieri.model.Account;
import ist.challenge.vieri.repository.AccountRepository;
import ist.challenge.vieri.service.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@DataJpaTest
public class AccountRepositoryTest {

  @Mock
  private AccountRepository accountRepository;

  @Mock
  private BCryptPasswordEncoder encoder;

  @InjectMocks
  private AccountServiceImpl accountService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  void testRegister() {
    AccountRequest request = new AccountRequest();
    request.setUsername("vieri");
    request.setPassword("vierifath");

    Account existingAccount = new Account();
    existingAccount.setUsername("vieri");

    when(accountRepository.findByUsername("vieri")).thenReturn(existingAccount);
    ResponseEntity<String> responseEntity = accountService.register(request);

    assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
  }

  @Test
  void testLogin() {
    AccountRequest request = new AccountRequest();
    request.setUsername("vieri");
    request.setPassword("vierifath");

    Account existingAccount = new Account();
    existingAccount.setUsername("vieri");
    existingAccount.setPassword("vierilama");

    when(accountRepository.findByUsername("vieri")).thenReturn(existingAccount);
    when(encoder.matches("vierifath", "vierilama")).thenReturn(true);

    ResponseEntity<String> responseEntity = accountService.login(request);

    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
  }

  @Test
  void testUpdate() {
    AccountRequest request = new AccountRequest();
    request.setUsername("vieri");
    request.setPassword("vierifath");

    Account existingAccount = new Account();
    existingAccount.setId(1L);
    existingAccount.setUsername("vieri");
    existingAccount.setPassword("pieripath");

    Optional<Account> optionalAccount = Optional.of(existingAccount);

    when(accountRepository.findById(1L)).thenReturn(optionalAccount);
    when(accountRepository.findByUsername("vieri")).thenReturn(existingAccount);
    when(encoder.matches("vierifath", "pieripath")).thenReturn(false);

    ResponseEntity<String> responseEntity = accountService.update(1L, request);

    assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
  }

}
